import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:map_launcher_ohos/map_launcher_ohos.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  const channel = MethodChannel('map_launcher_ohos');
  late List<MethodCall> log;

  setUp(() {
    log = <MethodCall>[];
    _ambiguate(TestDefaultBinaryMessengerBinding.instance)!
        .defaultBinaryMessenger
        .setMockMethodCallHandler(channel, (MethodCall methodCall) async {
      log.add(methodCall);

      return null;
    });
  });

  ///Not adapted to Windows, returned as null
  test('getInstalledMaps', () async {
    final maps = await channel.invokeMethod('getInstalledMaps');
    expect(maps, null);
  });

  ///Not adapted to Windows, returned as null
  test('isMapAvailable', () async {
    expect(await MapLauncher.isMapAvailable(MapType.petal), null);
  });
}

/// This allows a value of type T or T? to be treated as a value of type T?.
/// We use this so that APIs that have become non-nullable can still be used
/// with `!` and `?` on the stable branch.
T? _ambiguate<T>(T? value) => value;
